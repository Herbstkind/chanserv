/// This module contains all *host* modules, each one grouping similar bot functionality. They have
/// to be registered by calling [register_module](modules::register_module) on each one. After that
/// they are used by [`dispatch_message`](crate::dispatch_message) to dispatch messages to,
/// possibly handling them with their [`handle` method](modules::Module::handle).
mod modules;

//TODO:
//mod moderation; // Muting functionality like in Mee6
//mod housekeeping; // Pruning functionality (does the API have a pruning endpoint?)
//mod bridge; // In #moderation bescheid sagen, wenn jemand neues im IRC joint (ein Nick, der noch nicht in einer Datei steht) => Das lieber in einem separaten Bot machen?
//mod lurija; // Commands from Lurija
//mod quote; // Quoting functionality (commands from quote)

use modules::{gatekeeping::Gatekeeping, maintenance::Maintenance, register_module, ModuleChain};
use serenity::model::{
    channel::Message,
    gateway::Ready,
    guild::GuildStatus::*,
    id::GuildId,
    id::{ChannelId, RoleId},
};
use serenity::prelude::*;
use std::env;

/// Name of the channel to which error messages will be sent. Error messages are printed to stdout
/// regardless. See [`dispatch_message`](dispatch_message).
const ERROR_CHANNEL_NAME: &str = "errors";

/// Gets a token, creates a [client](serenity::prelude::Client), registers [modules](modules), and starts the client.
fn main() {
    // get token
    let token = env::var("DISCORD_TOKEN")
        .expect("Please supply token via DISCORD_TOKEN environment variable");

    // create client
    let mut client = Client::new(&token, Handler).expect("Error creating client");

    // custom variables
    let activation = Gatekeeping::new(
        "aktivieren",
        "Tulpa",
        "Du bist jetzt aktiviert. Viel Spaß!",
        vec!["aktiviert"],
    );
    let sensitive = Gatekeeping::new(
        "aktivieren",
        "Ja, ich fühle mich bereit, auch sensiblere Themen zu besprechen.",
        "Du bist jetzt für die Kanäle für sensible Themen freigeschaltet.",
        vec!["1337 h4xx0r", "Chrysalis"],
    );

    // register modules
    // register the Maintenance module first so that it can override message handling
    register_module(&mut client, Maintenance::new());
    register_module(&mut client, activation);
    register_module(&mut client, sensitive);

    // start client
    if let Err(e) = client.start() {
        panic!("Client error: {}", e);
    }
}

/// Handles events from the serenity API.
struct Handler;

impl EventHandler for Handler {
    fn cache_ready(&self, _: Context, guilds: Vec<GuildId>) {
        println!("The cache has received and inserted all data from the following guilds:");
        for guild_id in guilds {
            println!("- {}", guild_id);
        }
    }

    fn ready(&self, _: Context, ready: Ready) {
        println!("Client ready.");

        // show bot user
        println!("Current user: {}", ready.user.tag());

        // check that we're connected to exactly one guild
        if ready.guilds.is_empty() {
            panic!("Bot is not connected to any guild");
        }
        if ready.guilds.len() > 1 {
            panic!("Bot is connected to more than one guild");
        }

        // show guild info
        // TODO find and display name of guild (via Context?)
        let guild = ready.guilds.get(0).expect("Can't get guild");
        let guild_id = match guild {
            OnlinePartialGuild(g) => g.id,
            OnlineGuild(g) => g.id,
            Offline(g) => g.id,
            _ => panic!("Unknown guild status"),
        };
        println!("Connected to guild {}", guild_id);
    }

    fn message(&self, ctx: Context, msg: Message) {
        // ignore own messages!
        let bot_user = ctx.http.get_current_user().expect("Can't get bot user");
        if msg.author.id == bot_user.id {
            return;
        }

        // process others
        dispatch_message(ctx, msg);
    }
}

/// Called by the [`message`](serenity::prelude::EventHandler::message) handler implementation in
/// [`host::Handler`](crate::Handler). Dispatches messages to the registered
/// [modules](crate::modules) by calling their [`handle`](modules::Module::handle) methods in the
/// same order the modules [were registered](modules::register_module), until `Ok(true)` is
/// returned. In case of error it writes to stdout as well as a designated
/// [`ERROR_CHANNEL`](ERROR_CHANNEL_NAME).
fn dispatch_message(ctx: Context, msg: Message) {
    // modules have to be mutable because they may want to alter their state
    // e. g. putting the bot into different modes
    for module in ctx
        .data
        .write()
        .get_mut::<ModuleChain>()
        .expect("No modules registered for handling messages")
    {
        match module.handle(&ctx, &msg) {
            Ok(handled) => {
                if handled {
                    return;
                }
            }
            Err(e) => {
                let error_msg = format!("Handler of module {} failed: {}", module.get_name(), e);

                // print to stderr
                eprintln!("{}", error_msg);

                // print to error channel
                let guild_id = msg.guild_id.expect("Guild not found for message");
                let channel_id = get_channel_id(&ctx, guild_id, ERROR_CHANNEL_NAME)
                    .expect("Cannot find channel ID for name of error channel");
                channel_id
                    .say(&ctx.http, error_msg)
                    .expect("Cannot write to error channel");
            }
        }
    }
}

/// Returns the [`ChannelId`](serenity::model::id::ChannelId) of the channel with a given `channel_name` or `None` if no ID can be found.
fn get_channel_id(ctx: &Context, guild_id: GuildId, channel_name: &str) -> Option<ChannelId> {
    // Strip leading '#' from channel_name
    let channel_name = channel_name.trim_start_matches('#');

    // get channels from guild
    // FIXME: Cache result in static, update on channel_delete/channel_update/... callbacks. Probably move everything to the ready callback and get rid of this function.
    let channels = guild_id.channels(ctx).expect("No channels found in guild");

    // find the channel ID
    for c in channels.values() {
        if c.name == channel_name {
            return Some(c.id);
        }
    }

    // no channel ID found
    None
}

/// Returns the [`RoleId`](serenity::model::id::RoleId) of the role with a given `role_name` or `None` if no ID can be found.
fn get_role_id(ctx: &Context, guild_id: GuildId, role_name: &str) -> Option<RoleId> {
    // get roles from guild
    // FIXME: Cache result in static, update on guild_role_delete/guild_role_update/... callbacks. Probably move everything to the ready callback and get rid of this function.
    let roles = guild_id
        .to_partial_guild(ctx)
        .expect("Can't find guild for guild ID")
        .roles;

    // find the role ID
    for r in roles.values() {
        if r.name == role_name {
            return Some(r.id);
        }
    }

    // no channel ID found
    None
}
