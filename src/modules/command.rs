use crate::modules::Module;
use serenity::{model::channel::Message, prelude::*, Error, Result};
use std::process;

/// This module listens to a given `channel` and upon receiving a `message` with a given content,
/// starts a given command and waits for it to finish. If the command fails, the whole module
/// fails.
pub struct Command<'a> {
    /// Name of the channel to listen to.
    pub channel: &'a str,
    /// The message to listen for.
    pub message: &'a str,
    /// The command to start
    pub command: &'a str,
}

impl Command<'_> {
    pub fn new<'a>(channel: &'a str, message: &'a str, command: &'a str) -> Command<'a> {
        Command {
            channel,
            message,
            command,
        }
    }
}

impl Module for Command<'_> {
    fn get_name(&self) -> &'static str {
        "command"
    }

    fn handle(&mut self, ctx: &Context, msg: &Message) -> Result<bool> {
        // get name of channel
        let channel_name = msg
            .channel_id
            .name(ctx)
            .ok_or(Error::Other("No name found for channel"))?;

        // check that message is in the channel we're listening to
        if channel_name != self.channel || msg.content != self.message {
            return Ok(false);
        }

        // channel and message are correct, start command
        let output = process::Command::new(self.command).output()?;

        // check result
        if !output.status.success() {
            return Err(Error::Other("Command exited unsuccessfully"));
        }

        // done
        Ok(true)
    }
}
