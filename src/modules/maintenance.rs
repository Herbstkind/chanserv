use crate::modules::Module;
use serenity::{model::channel::Message, prelude::*, Error, Result};

/// User ID of the owner of this bot. Commands by other users are ignored.
const BOT_OWNER_ID: u64 = 321000161507540992;
/// The command used to invoke maintenance functionality. It is followed by one of several
/// subcommands. See [`dispatch_subcommand`](Maintenance::dispatch_subcommand) for details.
const MAINTENANCE_COMMAND: &str = "!maintenance";

/// Module for running maintenance and debugging tasks, like e.&nbsp;g. finding out the ID of a
/// user/channel/role.
pub struct Maintenance {
    /// Whether debugging mode is active. Debugging mode basically just means that all messages
    /// (and a few more actions than usual) will be explicitely logged to stdout.
    debugging_mode: bool,
}

impl Maintenance {
    pub fn new() -> Self {
        Self { debugging_mode: false }
    }

    /// Runs other functions according to the subcommand given. To see the list of subcommands run
    /// the subcommand `help` (see [`print_help`](crate::modules::maintenance::print_help)).
    fn dispatch_subcommand(&mut self, ctx: &Context, msg: &Message) -> Result<bool> {
        // split the message to get the subcommand
        let mut msg_parts = msg.content.as_str().split(' ');
        let subcommand = msg_parts
            .nth(1)
            .ok_or(Error::Other("No maintenance subcommand found"))?;

        // log subcommand
        if self.debugging_mode {
            println!("Maintenance subcommand: {}", subcommand);
        }

        // dispatch according to the subcommand
        // (sorted alphabetically)
        if "channel".starts_with(subcommand) {
            show_info_about_channel(msg);
        } else if "debugging".starts_with(subcommand) {
            self.toggle_debugging_mode();
        } else if "help".starts_with(subcommand) {
            print_help();
        } else if "user".starts_with(subcommand) {
            show_info_about_user(ctx, msg)?;
        } else {
            // subcommand could not be dispatched
            return Ok(false);
        }

        Ok(true)
    }

    /// Toggles debugging mode on/off.
    fn toggle_debugging_mode(&mut self) {
        self.debugging_mode = !self.debugging_mode;
        if self.debugging_mode {
            println!("Debugging mode on");
        } else {
            println!("Debugging mode off");
        }
    }
}

impl Module for Maintenance {

    fn get_name(&self) -> &'static str {
        "maintenance"
    }

    fn handle(&mut self, ctx: &Context, msg: &Message) -> Result<bool> {
        // log messages if in maintenance mode
        if self.debugging_mode {
            log_message(ctx, msg);
        }

        // check if message is relevant
        if !message_must_be_handled(msg) {
            return Ok(false);
        }

        // treat other messages as subcommands
        if self.dispatch_subcommand(ctx, msg)? {
            return Ok(true);
        }

        // unknown subcommand encountered
        println!("Warning: Maintenance command found, but unknown subcommand. Ignoring.");

        // proceed as normal, maybe some other module knows this subcommand
        Ok(false)
    }
}

/// Check whether a message must be handled by the maintenance module or be ignored.
fn message_must_be_handled(msg: &Message) -> bool {
    let mut msg_parts = msg.content.as_str().split(' ');
    // ignore messages that aren't maintenance commands
    match msg_parts.next() {
        None => return false,
        Some(prefix) => {
            // maintenance command must be at least two characters long ('!' and at least 'm')
            // and needs to be a portion of the start of MAINTENANCE_COMMAND
            if prefix.len() < 2 || !MAINTENANCE_COMMAND.starts_with(prefix) {
                return false;
            }
        }
    }

    // ignore messages by people other than the bot owner
    if msg.author.id != BOT_OWNER_ID {
        println!("User \"{}\" tried to use a maintenance command despite not being the bot owner. Ignoring.", msg.author.tag());
        return false;
    }

    // handle other messages
    true
}

/// Write information about a message to stdout. Print its message ID, guild ID, channel name,
/// author tag, and the content of the message. If the message has any attachments, list each
/// attachment's guild ID, message ID, attachment ID, filename, size in bytes, and its proxy URL.
/// This function should only be called when in [debugging mode](Maintenance::debugging_mode).
fn log_message(ctx: &Context, msg: &Message) {

    // print general message information
    let guild_id = msg
        .guild_id
        .map(|id| format!("{}", id))
        .unwrap_or_else(|| "?".into());
    if !msg.content.is_empty() {
        println!(
            "({}) {}: #{} <{}> {}",
            guild_id,
            msg.id,
            msg.channel_id.name(ctx).unwrap_or_else(|| "?".into()),
            msg.author.tag(),
            msg.content,
        );
    }

    // print attachments
    if !msg.attachments.is_empty() {
        for a in &msg.attachments {
            println!(
                "({}) {} attachment {}: \"{}\" ({} bytes): {}",
                guild_id, msg.id, a.id, a.filename, a.size, a.proxy_url
            );
        }
    }
}

/////////////////////////////
// maintenance subcommands //
/////////////////////////////

/// Print help about the available maintenance subcommands.
fn print_help() {
    println!("Maintenance module:");
    println!("  Command: {}", MAINTENANCE_COMMAND);
    println!("  Subcommands:");
    println!("  - channel: Show info about the current channel");
    println!("  - debugging: Toggle debugging mode on/off");
    println!("  - help: Show this help text");
    println!("  - user: Show info about the current user or a mentioned user");
    println!("  Subcommands don't have to be written out completely. E.&nbsp;g. 'c' suffices to run the 'channel' command.");
}

/// Show information about the given channel. For now that just means print its ID.
fn show_info_about_channel(msg: &Message) {
    // get channel
    let channel_id = match &msg.mention_channels {
        Some(cs) => {
            cs.iter()
                .next()
                .expect("Expected channel mention, but found none")
                .id
        }
        None => msg.channel_id,
    };

    // show info
    println!("Channel ID: {}", channel_id);
}

/// Show information about the given user. Print the user's tag, ID, and their roles.
fn show_info_about_user(ctx: &Context, msg: &Message) -> Result<()> {
    // get user to show (message author by default)
    let user = match msg.mentions.iter().next() {
        Some(u) => u,
        None => &msg.author,
    };

    // get roles from user
    let guild = msg
        .guild_id
        .ok_or(Error::Other("Cannot get guild ID of message"))?;
    let member = guild.member(ctx, user.id)?;

    // print roles
    println!("Roles of {} <{}>:", user.id, user.tag());
    for role in member
        .roles(ctx)
        .ok_or(Error::Other("Cannot get roles for user"))?
    {
        println!("  - {} \"{}\"", role.id, role.name);
    }

    Ok(())
}
