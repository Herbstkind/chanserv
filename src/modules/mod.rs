pub mod command;
pub mod gatekeeping; // Gatekeeping functionality (same as servitor)
pub mod maintenance;

use serenity::prelude::TypeMapKey;
use serenity::{
    client::{Client, Context},
    model::channel::Message,
    Result,
};

/// Every *host* module must implement this trait.
pub trait Module {
    /// Return a printable name of the module.
    fn get_name(&self) -> &'static str;

    /// This function is called by [`dispatch_message`](crate::dispatch_message). When the message
    /// can be handled by this module, it returns `Ok(true)`, else `Ok(false)`.
    fn handle(&mut self, ctx: &Context, msg: &Message) -> Result<bool>;
}

/// Represents a chain of modules registered via [`register_module`](register_module) that is used
/// by [`dispatch_message`](crate::dispatch_message) to call the modules' [`handle`
/// methods](Module::handle) one after another.
pub struct ModuleChain {}

impl TypeMapKey for ModuleChain {
    type Value = Vec<Box<dyn Module + Send + Sync>>;
}

/// Registers a module of type `M` in the `client`. The module instance is added to the client's
/// [`data`](serenity::client::Client::data). Call this function after [instanciating the
/// client](serenity::client::Client::new) and before [starting
/// it](serenity::client::Client::start).
pub fn register_module<M: Module + Send + Sync + 'static>(client: &mut Client, module: M) {
    println!("Registering instance of module {}", module.get_name());
    let mut data = client.data.write();
    let chain = match data.get_mut::<ModuleChain>() {
        Some(c) => c,
        None => {
            // No module has been registered yet, this is the first one.
            // The module chain has to be prepared first
            data.insert::<ModuleChain>(Vec::new());
            data.get_mut::<ModuleChain>()
                .expect("Can't get module chain even though it was just prepared")
        }
    };
    chain.push(Box::new(module));
}
