use crate::{get_role_id, modules::Module};
use serenity::{model::channel::Message, prelude::*, utils::MessageBuilder, Error, Result};

/// Implements the concept of a gatekeeper. It listens to a given `channel` and upon receiving a
/// `message` with a given content, responds with `response` and assigns a given set of `roles` to
/// the message's author. This can be used to force new guild members to 'activate' themselves
/// before participating, possibly requiring them to read a welcome text (e.&nbsp;g. including the
/// rules of the guild) in order to learn how to activate themselves.
pub struct Gatekeeping<'a> {
    /// Name of the channel to listen to.
    pub channel: &'a str,
    /// The message to listen for.
    pub message: &'a str,
    /// The response to send. It is always prepended by a mention of the activation message author.
    pub response: &'a str,
    /// Names of the roles to grant the message author.
    pub roles: Vec<&'a str>,
}

impl Gatekeeping<'_> {
    pub fn new<'a>(
        channel: &'a str,
        message: &'a str,
        response: &'a str,
        roles: Vec<&'a str>,
    ) -> Gatekeeping<'a> {
        Gatekeeping {
            channel,
            message,
            response,
            roles,
        }
    }
}

impl Module for Gatekeeping<'_> {
    fn get_name(&self) -> &'static str {
        "gatekeeper"
    }

    fn handle(&mut self, ctx: &Context, msg: &Message) -> Result<bool> {
        // get name of channel
        let channel_name = msg
            .channel_id
            .name(ctx)
            .ok_or(Error::Other("No name found for channel"))?;

        // check that message is in the channel we're listening to
        if channel_name != self.channel || msg.content != self.message {
            return Ok(false);
        }

        // start adding roles to user
        println!("Adding roles to user {}:", msg.author.tag());
        for role in &self.roles {
            println!("- {}", role);
        }

        // assign the role to the user
        let guild = msg
            .guild_id
            .ok_or(Error::Other("Cannot get guild ID of message"))?;
        let mut member = guild.member(ctx, msg.author.id)?;
        for role in &self.roles {
            let role_id = get_role_id(
                ctx,
                msg.guild_id.expect("Cannot find guild ID of message"),
                role,
            )
            .ok_or(Error::Other("Cannot find role ID for role"))?;
            member.add_role(&ctx.http, role_id)?;
        }

        // delete the activation message and write a response
        msg.delete(ctx)?;
        let response = MessageBuilder::new()
            .mention(&msg.author)
            .push(": ")
            .push(self.response)
            .build();
        msg.channel_id.say(&ctx.http, &response)?;

        Ok(true)
    }
}

impl<'a> TypeMapKey for Gatekeeping<'static> {
    type Value = Gatekeeping<'static>;
}
